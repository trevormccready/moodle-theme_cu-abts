<?php
/**
 * @package		theme_cuabts
 * @copyright	2016 Cornerstone University, www.cornerstone.edu
 * @author 		Trevor McCready
 * @license 	All rights reserved.
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version	=	2016071901;
$plugin->requires	=	2015051105; // Developed and tested for Moodle 2.9+
$plugin->release	=	'v2.9';
$plugin->maturity	=	MATURITY_BETA;
$plugin->component	=	'theme_cuabts';